provider "aws" {
  
}

variable "ami_name" {
    type=string
}

resource "aws_instance" "web" {
  instance_type = "t3.micro"
  ami = var.ami_name
  tags = {
    Name = "WebServer00751"
  }
   
}
