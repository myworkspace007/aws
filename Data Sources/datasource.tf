#Data Sources - API Call ENI Interface reading dynamically


data "aws_vpcs" "foo" {}

data "aws_vpc" "foo" {
  id = tolist(data.aws_vpcs.foo.ids)[1]
}

data "aws_subnet" "my_super_subnet" {
  vpc_id = data.aws_vpc.foo.id
  
  }

output "foo" {
  value = data.aws_vpcs.foo.ids
  
}

output "listofips" {

  value=data.aws_subnet.my_super_subnet.cidr_block
  
}

data "aws_network_interfaces" "mynics" {
}

data "aws_network_interface" "mynic" {
  id=data.aws_network_interfaces.mynics.ids[0]
}

#resource "aws_network_interface" "foo" {
  #subnet_id   = data.aws_subnet.my_super_subnet.id
  # private_ips = ["10.0.0.100"]

#}

output "sub" {
  value = data.aws_network_interfaces.mynics.ids[1]
}

resource "aws_instance" "web" {
  ami           = "ami-0f9fc25dd2506cf6d"
  instance_type = "t2.micro"
  tags = {
    Name = "WebServer"
  }
  network_interface {
    network_interface_id = data.aws_network_interface.mynic.id
    #subnet_id   = data.aws_subnet.my_super_subnet.id
    #private_ips = ["10.0.0.100"]
    device_index         = 0
  }
   
}