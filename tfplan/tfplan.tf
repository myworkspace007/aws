provider "aws" {

}

variable "myinstance" {
  
  default="t2.micro"
}

variable "myami" {
  default = "ami-0f9fc25dd2506cf6d"
}

resource "aws_instance" "web1" {
  instance_type = var.myinstance
  #ami = "ami-0f9fc25dd2506cf6d"
  ami=var.myami
  tags = {
    Name = "WebServer-Dev"
  }
 
}

resource "aws_instance" "web2" {
  instance_type = var.myinstance
  #ami = "ami-0f9fc25dd2506cf6d"
  ami=var.myami
  tags = {
    Name = "WebServer-Prod"
  }
 
}

resource "aws_instance" "web3" {
  instance_type = var.myinstance
  #ami = "ami-0f9fc25dd2506cf6d"
  ami=var.myami
  tags = {
    Name = "WebServer-Stage"
  }
 
}