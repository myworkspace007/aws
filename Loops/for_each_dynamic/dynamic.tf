 locals {
    vpc_subnets=[{
       subnet1={
         cidr_block = "10.0.1.0/24"
          }
     
     subnet2={
         cidr_block = "10.0.2.0/24"
         
       }
    }
    ]
 }
 

resource "aws_vpc" "myvpc" {
  cidr_block = "10.0.0.0/16"
  tags={
    name="MYVPC"
  }
  dynamic "subnetting" {
    for_each =local.vpc_subnets
    content {
      cidr_block=subnetting.value.cidr_block
      
  }
  }
  
}