output "myec2_ip" {
  value = aws_instance.web1[*].public_ip
}

output "myec2_pri_ip" {
  value = aws_instance.web1[*].private_ip
}

